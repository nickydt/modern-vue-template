module.exports = {
    parser: 'vue-eslint-parser',
    env: {
        browser: true,
        es2021: true,
        node: true,
    },
    extends: [
        'eslint:recommended',
        'plugin:vue/vue3-essential',
        'plugin:vue/vue3-recommended',
        'plugin:vue/vue3-strongly-recommended',
        '@vue/typescript/recommended',
        '@vue/eslint-config-typescript',
        '@vue/eslint-config-prettier',
        'prettier',
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
    ],
    parserOptions: {
        parser: '@typescript-eslint/parser',
        ecmaVersion: 'latest',
        ecmaFeatures: {
            jsx: false,
        },
    },
    plugins: ['vue', 'prettier', '@typescript-eslint'],
    rules: {
        indent: ['error', 4, { SwitchCase: 1 }],
        'linebreak-style': ['error', 'unix'],
        quotes: ['error', 'single'],
        semi: ['error', 'never'],
        'import/extensions': 'off',
        'import/no-extraneous-dependencies': [0, { 'packageDir ': './src/' }],
        'max-len': [
            'error',
            {
                code: 100,
                comments: 120,
                ignorePattern: 'class="([\\s\\S]*?)"|d="([\\s\\S]*?)"', // ignore classes or svg draw attributes
                ignoreUrls: true,
            },
        ],
        'vue/multi-word-component-names': 'off',
        'prettier/prettier': [
            'error',
            {},
            {
                usePrettierrc: true,
            },
        ],
        '@typescript-eslint/ban-ts-comment': 'off',
        '@typescript-eslint/ban-types': 'off',
        '@typescript-eslint/explicit-module-boundary-types': 'off',
        'no-undef': 'off',
    },
    settings: {
        'import/resolver': {
            node: {
                extensions: ['.js', '.jsx', '.vue', 'svg', '.ts', '.tsx'],
                moduleDirectory: ['node_modules', 'src/'],
            },
            alias: {
                extensions: ['.js', '.jsx', '.vue', 'svg', '.ts', '.tsx'],
                map: [
                    ['@', './src'],
                    ['@public', './src/public'],
                ],
            },
        },
    },
}
