import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'

export default defineConfig({
    plugins: [vue()],
    resolve: {
        alias: {
            '@': resolve(__dirname, './src'),
            '@public': resolve(__dirname, 'src/public'),
        },
    },
    server: {
        open: false,
        host: true,
        port: 3000,
    },
    publicDir: '/src/public',
    build: {
        manifest: true,
        outDir: './build',
        emptyOutDir: true,
        copyPublicDir: true,
        target: 'es2021',
        rollupOptions: {
            output: {
                entryFileNames: 'assets/[name].js',
                assetFileNames: 'assets/[name].[ext]',
            },
        },
    },
})
