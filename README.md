# Modern Vue Template



## What is it?

This is a basic Vue 3 project template. Uses Vite, TypeScript, Pinia, Tailwind, Eslint+Prettier  

## How to use it ?

```
yarn install
yarn run dev
```

***

## Misc

TODO: Add something here later ;)
