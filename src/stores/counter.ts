import { defineStore } from 'pinia'
import { CounterStoreState } from '@/types'
import { BIG } from '@/constants'

export const useCounterStore = defineStore('counter', {
    state: (): CounterStoreState => ({
        counter: 0,
    }),
    getters: {
        isBig: (state) => state.counter > BIG,
    },
    actions: {
        resetCounter() {
            this.counter = 0
        },
    },
})
