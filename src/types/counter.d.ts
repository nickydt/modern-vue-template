export interface CounterStoreState {
    counter: number
}

export interface CounterStore extends CounterStoreState {
    isBig: boolean
    resetCounter: Function
}
