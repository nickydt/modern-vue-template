import { createRouter, createWebHistory, Router } from 'vue-router'
import { routes } from './routes'

export const router: Router = createRouter({
    history: createWebHistory(),
    routes,
})

// You can add guards here
